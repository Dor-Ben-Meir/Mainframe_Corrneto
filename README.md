# Cornetto Project




[Test missile development video](https://www.youtube.com/watch?v=mi1j1UG9Nk4).



## Introduction
Using the ‘UAV Toolbox Support Package for PX4’, uORB messeges are usded to transmit data between the MAVLink module and the Simulink model.


## Requirements
- Preinstallation of PX4 Source Code (v1.10.2)
- MATLAB/Simulink R2021a + Simulnik_Models
	- ‘UAV Toolbox Support Package for PX4’

## Provided Samples:
In 'Simulnik_Models' folder:

Before running Mainframe_init.m,
Right-click on the 'Simulnik_Models' folder, and choose 'add the folder and it's subfolders to PATH'. 

|Simulink Model|Description|MATLAB Code<br>initialization|
|:--|:--|:--|
|Corneto_flight_program_control_ert_rtw|Main flight code |Mainframe_init.m|
|PILAN2021a.slx|PIL|Mainframe_init.m|
|full_controll.slx|Full control |Mainframe_init.m|
|PILEncoder.slx|Encoder Integration|Mainframe_init.m|


## Install Matlab in ubuntu (in this exapmple matlab 2021a is intalled):
[Install Matlab in ubuntu 18.04](https://linuxconfig.org/how-to-install-matlab-on-ubuntu-18-04-bionic-beaver-linux)

this guide assumes that you have already downloaded the Matlab installation package for the Linux operating system (matlab_R20XXa_glnxa64.zip) and save it into the ~/Downloads directory.

1. Enter the next commands:
```bash
sudo mkdir -p /usr/local/MATLAB/R2021a/
cd Downloads
mkdir matlab
unzip -q matlab_R2021a_glnxa64.zip -d matlab 
cd matlab
sudo ./install
```

2. Change Matlab’s licence if neccesry:
```bash
sudo chown -R $LOGNAME: /usr/local/MATLAB/R2021a
sudo chmod u+rwx -R  /usr/local/MATLAB/R2021a
/usr/local/MATLAB/R2021a/bin/activate_matlab.sh
```

3. In order to manually create the symbolic link
```bash
cd /usr/local/bin/
sudo ln -s /usr/local/MATLAB/R2021b/bin/matlab matlab
```


4. MATLAB preferences directory:
```bash
cd ~/.matlab 
sudo mkdir R2021b
sudo chmod a+w -R R2021b/
export MATLAB_PREFDIR= ~/.matlab/ # if needed
```

5. Error using opengl:
```bash
export MESA_LOADER_DRIVER_OVERRIDE=i965;matlab
```

Or (additionaly) you can change the EXEC in /usr/share/applications/matlab.desktop to:
```bash
Exec=env MESA_LOADER_DRIVER_OVERRIDE=i965 matlab -desktop
```


6. **Uninstall matlab in Linux**

There is no uninstaller for MathWorks products on Linux. To remove MATLAB, you simply need to delete the MATLAB installation following the instructions below:
```bash
rm -rf /usr/local/MATLAB/R2021a
rm /usr/local/bin/matlab /usr/local/bin/mcc /usr/local/bin/mex /usr/local/bin/mbuild
```



## PX4 Quick Installation:
In order to install PX4 and its dependencies on UBUNTU 18.04:
```bash
cd PX4_Instaltion
sudo chmod u+x deploy_px4_ubuntu_18.sh
sudo ./deploy_px4_ubuntu_18.sh
```

Building  the code for Nuttx:
```bash
cd Firmware
sudo make px4_fmu-v3_default
```

Uploading Firmware (Flashing the board):
```bash
cd Firmware
sudo make px4_fmu-v3_default upload
```

## Qgroundcontrol:
Before installing QGroundControl for the first time:

```bash
sudo usermod -a -G dialout $USER
sudo apt-get remove modemmanager -y
sudo apt install gstreamer1.0-plugins-bad gstreamer1.0-libav gstreamer1.0-gl -y
```

Download QgroundControl.AppImage. From:
[Qgroundcontrol v3.5.6](https://github.com/mavlink/qgroundcontrol/releases/tag/v3.5.6)

Install (and run) using the terminal commands:
```bash
cd Downloads/ 
chmod +x ./QGroundControl.AppImage
./QGroundControl.AppImage  #(or double click)
```





